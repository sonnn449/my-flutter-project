class ApiUrl {
  // Base:
  static const apiUrl = 'https://https://dominhduong.weuptech.vn/cms/api/v1/';

  // Login:

  // Location:
  static const apiLocationProvinceUrl = '$apiUrl/location-list-province';
  static const apiLocationDistrictUrl = '$apiUrl/location-list-district';
  static const apiLocationWardUrl = '$apiUrl/location-list-ward';
}

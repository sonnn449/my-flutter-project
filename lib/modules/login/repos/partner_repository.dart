import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:my_flutter_project/commons/constants/api_url.dart';
import 'package:my_flutter_project/modules/login/models/partner_model.dart';
import 'package:dio/dio.dart';

const assetKey = 'assets/posts.json';

class ListPartnerRepo {
  Future<List<Partner>?> getListPartner() async {
    try {
      final results = await Dio().get(ApiUrl.apiLocationProvinceUrl);
      return results.data.map((json) => Partner.fromJson(json)).toList();
    } catch (e) {
      rethrow;
    }
  }
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'partner_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Partner _$PartnerFromJson(Map<String, dynamic> json) => Partner(
      id: json['id'] as int?,
      name: json['name'] as String?,
    );

Map<String, dynamic> _$PartnerToJson(Partner instance) => <String, dynamic>{
      'name': instance.name,
      'id': instance.id,
    };

ListPartners _$ListPartnersFromJson(Map<String, dynamic> json) => ListPartners(
      (json['items'] as List<dynamic>)
          .map((e) => Partner.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$ListPartnersToJson(ListPartners instance) =>
    <String, dynamic>{
      'items': instance.results,
    };

import 'package:json_annotation/json_annotation.dart';
part 'partner_model.g.dart';

@JsonSerializable()
class Partner {
  final String? name;
  final int? id;

  Partner({this.id, this.name});

  factory Partner.fromJson(Map<String, dynamic> json) =>
      _$PartnerFromJson(json);
}

@JsonSerializable()
class ListPartners {
  final List<Partner> results;

  ListPartners(this.results);

  factory ListPartners.fromJson(Map<String, dynamic> json) =>
      _$ListPartnersFromJson(json);
}
